"""
The goal of this exercise is to get acquainted with the machine learning building blocks.
There are many methods and algorithms to train a machine learning model, but most of them share the same structure:
1. Load the data and preprocess it
2. Define the hypothesis and cost function
3. Design a learning algorithm (e.g. gradient descent)
4. Set the training hyperparameters (learning rate, number of iterations/epochs, etc.)
5. String all of the above to perform training
6. Evaluate your model and repeat (next week)
"""

# import the packages you plan on using
import numpy as np
import pandas as pd

from sklearn import preprocessing, linear_model
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt


def hypothesis(X, theta):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :return H: (numpy array or scalar) Model's output on the sample set
    """
    H = np.dot(X, theta.T)
    return H


def cost(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground gradtruth for each sample)
    :return cost: (scalar) The model's parameters mean loss for all samples
    """
    N = y.shape[0]
    cost = np.sum(np.power(hypothesis(X, theta) - y, 2))
    return cost / N


def gradient(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return gradient: (numpy array or scalar) parameter gradients for current step
    """
    N = y.shape[0]
    grad = np.dot((hypothesis(X, theta) - y).T, X) * 2

    return grad / N


def batch_gradient_descent(X, y, theta, lr, batch_size, num_epochs):
    """
    :param X: (numpy array) rows: examples ,  cols: features
    :param y: (numpy array) target vector
    :param theta: (numpy array) model parameters
    :param lr: (float) learning rate
    :param batch_size: (int) training batch size per iteration
    :param num_epochs: (int) total number of passes through the data
    :return theta: (numpy array) optimized model parameters
    """
    # determine how many steps are needed per epoch
    steps = int(np.ceil(X.shape[0] / batch_size))
    # start a loop over the number of epochs
    for i in range(num_epochs):
        # start a loop over the number of steps required to finish an epoch
        for step in range(0, steps):
            # fetch the next batch from the data
            data = X[step * batch_size:(step + 1) * batch_size]
            label = y[step * batch_size:(step  + 1) * batch_size]
            # compute the gradient for this batch
            grad = gradient(data, theta, label)
            # update the model parameters according to learning rate
            theta -= (lr * grad)
        # print the cost every epoch
        print 'epoch ',i,': ',cost(X, theta, y)
    # return the optimized model parameters
    return theta


def main():
    # load the WW2 weather data
    df = pd.read_csv('data/weatherww2.csv')
    # shuffle the data
    df = df.sample(frac=1)
    # add bias column to data
    df['bias'] = 1
    # prepare it for training (grab only relevant columns, split to X (samples) and y (target)
    df[['MinTemp', 'MaxTemp', 'bias']]
    # normalize the data
    df['MinTemp'] = preprocessing.scale(df['MinTemp'])
    #df['bias'] = preprocessing.scale(df['bias'])
    X = df[['MinTemp', 'bias']]
    y = df['MaxTemp'].as_matrix().reshape(-1, 1)
    # initialize theta (model parameters)
    theta = np.random.rand(1, X.shape[1])
    # configure training hyperparameters
    batch_size = 64
    learning_rate = 0.1
    num_epochs = 10
    # start training and return optimal parameters
    opt = batch_gradient_descent(X, y, theta, learning_rate, batch_size, num_epochs)
    # train with scikit-learn and compare your results

    print 'My linear regression coefficients: \n', opt
    print ("Mean squared error: %.2f"
           % mean_squared_error(y, np.dot(X, opt.T)))
    regr = linear_model.LinearRegression()
    regr.fit(X, y)
    print 'scikit-learn coefficients: \n', regr.coef_
    y_pred = regr.predict(X)
    print("Mean squared error: %.2f"
          % mean_squared_error(y, y_pred))

    plt.scatter(X.MinTemp.as_matrix(), y, color='black')
    plt.plot(X.MinTemp, np.dot(X, opt.T), color='blue', linewidth=3)
    plt.plot(X.MinTemp, y_pred, color='red', linewidth=3)
    plt.xticks(())
    plt.yticks(())

    plt.show()


if __name__ == '__main__':
    main()
