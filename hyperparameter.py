"""
The goal of this exercise is to get acquainted with the machine learning building blocks.
There are many methods and algorithms to train a machine learning model, but most of them share the same structure:
1. Load the data and preprocess it
2. Define the hypothesis and cost function
3. Design a learning algorithm (e.g. gradient descent)
4. Set the training hyperparameters (learning rate, number of iterations/epochs, etc.)
5. String all of the above to perform training
6. Evaluate your model and repeat (next week)
"""

# import the packages you plan on using
import numpy as np
import pandas as pd
from scipy import stats

from sklearn import preprocessing, linear_model
from sklearn.metrics import mean_squared_error

from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels


class ReduceLR():

    def __init__(self, loss, factor=0.2, patience=5, min_lr=0.001, lr=0.1):
        self.factor = factor
        self.patience = patience
        self.min_lr = min_lr
        self.loss = loss
        self.cooldown = self.patience
        self.min_delta = 0.02
        self.lr = lr

    def loss_append(self,loss):
        self.loss.insert(0, loss)

    def isSmall(self):
        if self.lr < self.min_lr:
            self.lr = self.min_lr
            print 'learning rate too small! new learning rate set to ', self.min_lr
            return self.min_lr
        else:
            return self.lr

    def update(self):
        if self.cooldown:
            self.cooldown -= 1
            return self.lr
        else:
            if len(self.loss) <= self.patience:
                return self.lr
            else:
                history = np.array(self.loss[1:1+self.patience]).mean()
                # print history, self.loss[-1], np.abs(self.loss[-1]-history)
                #print np.array(self.loss[1:1+self.patience]), 'last item: ', self.loss[0].ravel(), ' mean: ', np.array(self.loss[1:1+self.patience]).mean(),'delta :', np.abs(self.loss[0]-history).ravel()
                delta = np.abs(self.loss[0]-history).ravel()
                if delta < self.min_delta:
                    print "updating lr: ", self.lr * self.factor
                    self.cooldown = self.patience
                    self.lr *= self.factor
                return self.isSmall()



def sigmoid(X):
    return 1 / (1 + np.exp(-X))


def hypothesis(X, theta):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :return H: (numpy array or scalar) Model's output on the sample set
    """
    H = np.dot(X, theta.T)
    H = sigmoid(H)
    return H


def cost(X, theta, y, l2):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground gradtruth for each sample)
    :return cost: (scalar) The model's parameters mean loss for all samples
    """
    N = y.shape[0]
    h = hypothesis(X, theta)
    return (-y * np.log(h) - (1 - y) * np.log(1 - h)).mean() + (l2/2 * np.dot(theta, theta.T).ravel())


def gradient(X, theta, y, l2):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return gradient: (numpy array or scalar) parameter gradients for current step
    """
    N = y.shape[0]
    h = hypothesis(X, theta)
    grad = (np.dot((h - y).T, X) / N) + (l2 * theta)
    return grad



def batch_gradient_descent(Xtr, ytr, Xte, yte, theta, lr, l2, batch_size, num_epochs, momentum):
    """
    :param np.ndarray X: (numpy array) rows: examples ,  cols: features
    :param y: (numpy array) target vectorgrad.shape
    :param theta: (numpy array) model parameters
    :param lr: (float) learning rate
    :param batch_size: (int) training batch size per iteration
    :param num_epochs: (int) total number of passes through the data
    :return theta: (numpy array) optimized model parameters
    """


    test_loss = []
    train_loss =[]
    policy = ReduceLR([])
    # determine how many steps are needed per epoch
    steps = int(np.ceil(Xtr.shape[0] / batch_size))
    old_grad=np.zeros(theta.shape)
    # start a loop over the number of epochs
    for i in range(num_epochs):
        # start a loop over the number of steps required to finish an epoch
        for step in range(0, steps):
            # fetch the next batch from the data
            data = Xtr[step * batch_size:(step + 1) * batch_size]
            label = ytr[step * batch_size:(step  + 1) * batch_size]
            # compute the gradient for this batch
            grad = gradient(data, theta, label, l2)
            grad = momentum*grad + (1-momentum)*old_grad
            old_grad=grad
            theta -= (lr * grad)

            # update the model parameters according to learning rate

        # print the cost every epoch
        print 'epoch ',i+1,'/',num_epochs,': ',cost(Xte, theta, yte, l2).ravel()
        policy.loss_append(cost(Xte, theta, yte, l2))
        lr = policy.update()
        test_loss.append(cost(Xte, theta, yte, l2))
        #train_loss.append(cost(Xtr, theta, ytr, l2))

    # return the optimized model parameters
    policy.loss.reverse()
    train_loss = policy.loss
    l_curv(train_loss, test_loss)
    return theta

def predict(X, theta, threshold=0.5):
    x = predict_probs(X, theta) >= threshold
    return [int(val) for val in x]

def predict_probs(X, theta):
    return sigmoid(np.dot(X, theta.T))


def data_preprocess(df):
    df = df.drop(columns=['Sunshine', 'Evaporation', 'Cloud3pm', 'Cloud9am', 'Location', 'RISK_MM', 'Date'], axis=1)
    df = df.dropna(how='any')
    z = np.abs(stats.zscore(df._get_numeric_data()))
    df = df[(z < 3).all(axis=1)]
    df['RainToday'].replace({'No': 0, 'Yes': 1}, inplace=True)
    df['RainTomorrow'].replace({'No': 0, 'Yes': 1}, inplace=True)
    categorical_columns = ['WindGustDir', 'WindDir3pm', 'WindDir9am']
    df = pd.get_dummies(df, columns=categorical_columns)
    model = preprocessing.MinMaxScaler()
    model.fit(df)
    df = pd.DataFrame(model.transform(df), index=df.index, columns=df.columns)
    return df

def l_curv (train_loss, test_loss):
    epoch = np.arange(len(train_loss))
    fig = plt.figure()
    plt.plot(epoch, train_loss, c='b')
    plt.plot(epoch, test_loss, c='r')
    plt.xlabel('epoch #')
    plt.ylabel('loss/loss')
    plt.title('learning curve graph')
    plt.show(block=False)





def main():
    # load the WW2 weather data
    df = pd.read_csv('data/weatherAUS.csv')
    df = data_preprocess(df)
    # shuffle the data
    df = df.sample(frac=1)
    # add bias column to data
    # prepare it for training (grab only relevant columns, split to X (samples) and y (target)
    y = df[['RainTomorrow']]
    X = df.loc[:, df.columns != 'RainTomorrow']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
    # normalize the data
    #df['bias'] = preprocessing.scale(df['bias'])

    theta = np.random.rand(1, X.shape[1])
    # configure training hyperparameters
    batch_size = 64
    learning_rate = 0.1
    num_epochs = 25
    # start training and return optimal parameters
    opt = batch_gradient_descent(X_train, y_train, X_test, y_test, theta, learning_rate, 0.0, batch_size, num_epochs, 0.8)
    print 'My linear regression coefficients: \n', opt
    y_test = y_test.as_matrix()
    y_pred = predict(X_test, theta)
    count = len(["ok" for idx, label in enumerate(y_test) if label == y_pred[idx]])
    print 'Acc score: ', float(count)/len(y_test)
    plot_confusion_matrix(y_test, y_pred)
    plt.show()

def plot_confusion_matrix(y_true, y_pred,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = unique_labels(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

if __name__ == '__main__':
    main()

