"""
The goal of this exercise is to get acquainted with the machine learning building blocks.
There are many methods and algorithms to train a machine learning model, but most of them share the same structure:
1. Load the data and preprocess it
2. Define the hypothesis and cost function
3. Design a learning algorithm (e.g. gradient descent)
4. Set the training hyperparameters (learning rate, number of iterations/epochs, etc.)
5. String all of the above to perform training
6. Evaluate your model and repeat (next week)
"""

# import the packages you plan on using
import numpy as np
import pandas as pd

from sklearn import preprocessing, linear_model
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt

def sigmoid(X):
    return 1 / (1 + np.exp(-X))

def hypothesis(X, theta):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :return H: (numpy array or scalar) Model's output on the sample set
    """
    H = np.dot(X, theta.T)
    H = sigmoid(H)
    return H


def cost(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground gradtruth for each sample)
    :return cost: (scalar) The model's parameters mean loss for all samples
    """
    N = y.shape[0]
    h = hypothesis(X, theta)
    return (-y * np.log(h) - (1 - y) * np.log(1 - h)).mean()



def gradient(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return gradient: (numpy array or scalar) parameter gradients for current step
    """
    N = y.shape[0]
    h = hypothesis(X, theta)
    grad = np.dot((h - y).T, X) / N
    return grad



def batch_gradient_descent(X, y, theta, lr, batch_size, num_epochs):
    """
    :param np.ndarray X: (numpy array) rows: examples ,  cols: features
    :param y: (numpy array) target vector
    :param theta: (numpy array) model parameters
    :param lr: (float) learning rate
    :param batch_size: (int) training batch size per iteration
    :param num_epochs: (int) total number of passes through the data
    :return theta: (numpy array) optimized model parameters
    """

    # determine how many steps are needed per epoch
    steps = int(np.ceil(X.shape[0] / batch_size))
    # start a loop over the number of epochs
    for i in range(num_epochs):
        # start a loop over the number of steps required to finish an epoch
        for step in range(0, steps):
            # fetch the next batch from the data
            data = X[step * batch_size:(step + 1) * batch_size]
            label = y[step * batch_size:(step  + 1) * batch_size]
            # compute the gradient for this batch
            grad = gradient(data, theta, label)
            # update the model parameters according to learning rate
            theta -= (lr * grad)
        # print the cost every epoch
        print 'epoch ',i,': ',cost(X, theta, y)
    # return the optimized model parameters
    return theta

def predict(X, theta, threshold=0.5):
    return predict_probs(X, theta) >= threshold

def predict_probs(X, theta):
    return sigmoid(np.dot(X, theta.T))

def main():
    # load the WW2 weather data
    df = pd.read_csv('data/data_banknote_authentication.csv')
    # shuffle the data
    df = df.sample(frac=1)
    # add bias column to data
    df['bias'] = 1
    # prepare it for training (grab only relevant columns, split to X (samples) and y (target)
    df[['variance', 'skewness', 'curtosis', 'entropy', 'bias', 'authenticity']]
    # normalize the data
    #df['bias'] = preprocessing.scale(df['bias'])
    X = df[['variance', 'skewness', 'curtosis', 'entropy', 'bias']]
    y = df['authenticity'].as_matrix().reshape(-1, 1)
    # initialize theta (model parameters)
    theta = np.random.rand(1, X.shape[1])
    # configure training hyperparameters
    batch_size = 64
    learning_rate = 0.11
    num_epochs = 100
    # start training and return optimal parameters
    opt = batch_gradient_descent(X, y, theta, learning_rate, batch_size, num_epochs)
    print 'My linear regression coefficients: \n', opt
    y_pred = predict(X, theta)
    count = len(["ok" for idx, label in enumerate(y) if label == y_pred[idx]])
    print 'Acc score: ', float(count)/len(y)

if __name__ == '__main__':
    main()

